import {Express, NextFunction, Request, Response, Router} from 'express';
import {ParamsDictionary, RequestHandler, RequestHandlerParams} from 'express-serve-static-core';
import {GenericServerError, HttpError, ResponseHelper} from '../util';
import {ParsedQs} from 'qs';

const prodEnv = process.env.NODE_ENV == 'production';

// Override normal express request handler to return a promise
export interface AsyncRequestHandler<T = any, P = ParamsDictionary, ResBody = any, ReqBody = any, ReqQuery = ParsedQs> {
  // tslint:disable-next-line callable-types (This is extended from and can't extend from a type alias in ts<2.2
  (req: Request<P, ResBody, ReqBody, ReqQuery>, res: Response<ResBody>, next: NextFunction): Promise<T>;
}

/**
 * Wrap the express router in a custom class for easier error handling and sending responses.
 *
 * @author Stefan Breetveld
 */
export class RouteWrapper {
  private readonly router: Router;

  /**
   * construct a new instance with a relative path.
   *
   * @param path relative path to this resource
   */
  constructor(private path: string) {
    this.router = Router();
  }

  /**
   * Register middleware on the router to intercept incoming traffic.
   *
   * @param middleware the middleware function used to intercept and/or relay the request
   * @param path the relative path this middleware should act on
   */
  registerMiddleware(middleware: RequestHandler, path: string = '*') {
    this.router.use(path, middleware);
  }

  /**
   * HTTP GET. Accept get requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   */
  get(route: string, handler: AsyncRequestHandler) {
    this.router.route(route).get(this.createRouteHandler(handler));
  }

  /**
   * HTTP POST. Accept post requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   */
  post(route: string, handler: AsyncRequestHandler) {
    this.router.route(route).post(this.createRouteHandler(handler));
  }

  /**
   * HTTP PUT. Accept put requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   */
  put(route: string, handler: AsyncRequestHandler) {
    this.router.route(route).put(this.createRouteHandler(handler));
  }

  /**
   * HTTP DELETE. Accept delete requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   */
  delete(route: string, handler: AsyncRequestHandler) {
    this.router.route(route).delete(this.createRouteHandler(handler));
  }

  /**
   * Add the given routeWrapper as sub routes of this RouteWrapper.
   *
   * @param routeWrapper a routeWrapper with sub routes.
   */
  subRoutes(routeWrapper: RouteWrapper) {
    this.router.use(routeWrapper.path, routeWrapper.router);
  }

  /**
   * Register this router to the given express app on the correct sub route.
   *
   * @param app the express app.
   * @param root the root of your endpoints
   */
  registerRoutes(app: Express, root: string = '') {
    app.use(`${root}${this.path}`, this.router);
  }

  /**
   * Convert a Handler into a method that can be used by express. Any errors will be handled here.
   *
   * @param handler the method that will process the requested resource.
   */
  private createRouteHandler(handler: AsyncRequestHandler): RequestHandlerParams {
    return async (req: Request, res: Response, next: NextFunction) => {
      handler(req, res, next)
        .then((result: any) => ResponseHelper.sendResponse(req, res, result))
        .catch((e: any) => {
          if (!(e instanceof HttpError)) {
            e = prodEnv ? new GenericServerError() : new GenericServerError({error: e});
          }
          ResponseHelper.sendError(req, res, e);
        });
    };
  }
}
