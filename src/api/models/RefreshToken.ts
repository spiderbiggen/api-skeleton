import {Column, Entity, getRepository, JoinColumn, ManyToOne, Repository} from 'typeorm';
import {DatedEntity} from './DatedEntity';
import {User} from './User';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('refresh_token')
export class RefreshToken extends DatedEntity {

  @Column({unique: true, length: 512})
  token: string;
  @ManyToOne(() => User, user => user.tokens)
  @JoinColumn({name: 'user_id'})
  user: User;
  @Column()
  expires: Date;
  @Column({default: false})
  disabled: boolean;


  static get repository(): Repository<RefreshToken> {
    return getRepository(RefreshToken);
  }

}
