import {CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

export abstract class KeyedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
}

export abstract class DatedEntity extends KeyedEntity {
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
