import UserRoutes from './UserRoutes';

/**
 * Export a list of all routes
 */
export default [UserRoutes];
