import {MigrationInterface, QueryRunner} from 'typeorm';

export class Users1574811302426 implements MigrationInterface {
  name = 'Users1574811302426';

  public async up(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query(
        `CREATE TABLE "user"
         (
           "id"         varchar(36)              NOT NULL DEFAULT uuid_generate_v4(),
           "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
           "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
           "username"   varchar(255)             NOT NULL,
           "email"      varchar(255)             NOT NULL,
           "password"   varchar(255)             NOT NULL,
           "roles"      text                     NOT NULL,
           CONSTRAINT "IDX_055984372b62f71f274f3fe361" UNIQUE ("username"),
           CONSTRAINT "IDX_a1689164dbbcca860ce6d17b2e" UNIQUE ("email"),
           PRIMARY KEY ("id")
         )`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query('DROP TABLE "user"', undefined);
  }

}
