import {MigrationInterface, QueryRunner} from 'typeorm';

export class RefreshTokens1574811328557 implements MigrationInterface {
  name = 'RefreshTokens1574811328557';

  public async up(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query(
        `CREATE TABLE "refresh_token"
         (
           "id"         varchar(36)              NOT NULL DEFAULT uuid_generate_v4(),
           "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
           "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
           "token"      varchar(512)             NOT NULL,
           "expires"    TIMESTAMP WITH TIME ZONE NOT NULL,
           "disabled"   boolean                  NOT NULL DEFAULT FALSE,
           "user_id"    varchar(36)              NULL,
           CONSTRAINT "IDX_4856449925e1c78455f9dc2e25" UNIQUE ("token"),
           PRIMARY KEY ("id")
         )`
    );
    // language=PostgreSQL
    await queryRunner.query(
        `ALTER TABLE "refresh_token"
        ADD CONSTRAINT "FK_e16634aa96ff8f19fddb8bf1f99" FOREIGN KEY (user_id) REFERENCES "user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query('DROP TABLE "refresh_token"', undefined);
  }
}
