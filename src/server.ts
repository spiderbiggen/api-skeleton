import express, {Request, Response} from 'express';
import 'reflect-metadata';
import {createConnection} from 'typeorm';
import {ROUTE_REGISTRY} from './routing';
import routes from './api/routes';
import {Authentication, LOGGER} from './util';
import bodyParser from 'body-parser';
import compression from 'compression';
import helmet from 'helmet';
import cors from 'cors';

const port = process.env.PORT || 3000;
const prodEnv = process.env.NODE_ENV === 'production';

/**
 * Create a database connection then load all routes. Then create the server.
 *
 * @author Stefan Breetveld
 */
createConnection()
  .then(() => ROUTE_REGISTRY.load(routes))
  .then(() => {
    const app = express();
    app.use(compression());
    app.use(helmet());
    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    // Get User from token if available, for all routes
    app.use(Authentication.userMiddleware);

    app.set('json spaces', prodEnv ? 0 : 2);

    const root = process.env.NODE_ROOT || '';
    ROUTE_REGISTRY.registerRoutes(app, root);
    app.use((req: Request, res: Response) => {
      LOGGER.info(`===================================================`);
      LOGGER.info(`PATH: ${req.originalUrl}`);
      LOGGER.info(`METHOD: ${req.method}`);
      LOGGER.info(`HEADERS: ${JSON.stringify(req.headers)}`);

      LOGGER.info(`BODY:`);
      LOGGER.info(JSON.stringify(req.body));

      res.sendStatus(204);
    });

    app.listen(port);
    LOGGER.info('API Running on :%s/%s', port, root ?? '');
  }).catch(err => {
  LOGGER.error(err);
});

