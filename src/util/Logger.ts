import {createLogger, format, transports} from 'winston';

export const DEBUG = 'debug';
export const VERBOSE = 'verbose';
export const INFO = 'info';
export const WARN = 'warn';
export const ERROR = 'error';

const colorizer = format.colorize({
  colors: {
    debug: 'green',
    verbose: 'gray',
    info: 'white',
    warn: 'yellow',
    error: 'red',
  }
});

const myFormat = format.printf(
  ({ level, message, timestamp }) => {
    return colorizer.colorize(level, `${timestamp} ${level}: ${message}`);
  });

export const LOGGER = createLogger({
  level: DEBUG,
  format: format.combine(
    format.timestamp(),
    format.splat(),
    myFormat
  ),
  defaultMeta: { service: 'user-service' },
  transports: [
    new transports.Console({level: DEBUG}),
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.File({ filename: 'error.log', level: ERROR }),
    new transports.File({ filename: 'combined.log', level: INFO })
  ]
});
