const tagsToReplace: { [key: string]: string } = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
};

export function escape(text: string): string {
  const pattern = `[${Object.keys(tagsToReplace).join()}]`;
  return text.replace(new RegExp(pattern, 'g'), tag => tagsToReplace[tag] ?? tag);
}

const BASE64_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

export function generateBase64Salt(length: number): string {
  let result = '';
  do {
    result += BASE64_CHARS.charAt(Math.floor(Math.random() * BASE64_CHARS.length));
  } while (result.length != length);
  return result;
}

export function toBoolean(value?: string | number): boolean {
  if (!value) {
    return false;
  }
  if (typeof value === 'string') {
    value = value.toLowerCase();
  }
  switch (value) {
    case 'y':
    case 'yes':
    case 't':
    case 'true':
    case '1':
      return true;
    default:
      return false;
  }
}
